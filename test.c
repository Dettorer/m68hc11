#include <stdlib.h>
#include <stdio.h>
#include "libs/hashtbl.h"
#include "libs/parser.h"

int main()
{
    FILE* file = fopen("test.in", "r");
    if(file == NULL)
	return -1;

    metaprog* meta = parse(file);
    metaprog_destroy(meta);

    fclose(file);
    return 0;
}
