#ifndef _PARSER_H_
# define _PARSER_H_

#define ERROR(msg, line) printf("At line %i: %s\n", line, msg)

#include <stdio.h>
#include "hashtbl.h"

/* All supported instruction's mnemonics */
typedef enum
{
    /* Accumulators */
    LDAA,
    LDAB,

    STAA,
    STAB,

    /* Add / Sub */
    ADDA,
    ADDB,
    ADDD,

    SUBA,
    SUBB,
    SUBD,

    /* Branching */
    BCC,
    BEQ,
    BNE,

    /* CMP */
    CMPA,
    CMPB,

    /* Logical shifts */
    LSR,
    LSRA,
    LSRB,
    LSRD,

    LSLA,
    LSLB,
    LSLD,

    /* Rotations */
    ROL,
    ROLA,
    ROLB,
    
    /* Others */
    ROR,

    /* Assembler directives */
    NAME,
    ORG,
    END,

    LABEL_ONLY
} inst_mnem;

/* Addressing modes */
typedef enum
{
    IMMEDIATE,
    DIRECT,
    EXTENDED,
    INDEXED_X,
    INDEXED_Y,
    RELATIVE,
    INHERANT
} addr_mode;

/* Instruction representation */
typedef struct inst
{
    char label[MAX_LABEL_SIZE];
    char name[6];
    inst_mnem mnem;
    addr_mode mode;
    int assembly;
    int data;
    struct inst* next;
} inst;

/* Various informations */
typedef struct
{
    int lc;
    hashtbl* labels;
    int org;
    char name[256];
    inst* start_prog; /* first instruction */
    inst* end_prog; /* last instruction */
} metaprog;

metaprog* parse(FILE* file);
void metaprog_destroy(metaprog* meta);
void meta_print(metaprog* meta);
int get_word(char** buffer, int* buffer_size, char* line, int* pos);

#endif
