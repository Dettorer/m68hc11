#ifndef INSTR_H
# define INSTR_H
#include "parser.h"

/* Gloabal hashtable containing instructions informations */
hashtbl* OPCODES;

/* Description of an instruction and it's S19 opcodes */
typedef struct 
{
    char name[6];
    inst_mnem mnem;
    int narg;

    int imm;
    int dir;
    int ext;
    int indx;
    int indy;
    int rel;
    int inh;
} opcode;

void make_inst();

#endif
