#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "hashtbl.h"

/* Calculate the hash of a data string */
int _hash(int size, const char* data)
{
    char c;
    int carry = 4242;
    while ((c = (*data++)))
	carry = (c + ( carry + (carry << 5))) % size;
    return carry;
}

/* Create a new hashtable */
hashtbl* hashtbl_create(int size)
{
    hashtbl* ht = malloc(sizeof(hashtbl));
    ht->size = size;

    node** table = calloc(size, sizeof(node*));
    ht->table = table;

    return(ht);
}

/* Free the memory used by an hashtable */
void hashtbl_destroy(hashtbl* ht)
{
    for(int i = 0; i < ht->size; i++)
    {
	/* Free all nodes of the collision list */
	node* n = ht->table[i];
	while(n != NULL)
	{
	    node* temp = n;
	    free(n->data);
	    n = n->next;
	    free(temp);
	}
    }
}

/* Prepare a new node and fill it with a key and a data */
node* _create_node(const char* key, void* data, size_t data_size)
{
    node* new = malloc(sizeof(node));

    strcpy(new->key, key);
    new->data = malloc(data_size);
    memcpy(new->data, data, data_size);
    new->next = NULL;

    return new;
}

/* Add a new element in the hashable, return 0 if the key is already used */
int hashtbl_add(hashtbl* ht, const char* key, void* data, size_t data_size)
{
    /* Create the new node */
    int hash = _hash(ht->size, key);
    node* new = _create_node(key, data, data_size);

    /* Find the place for this new node */
    node* point = ht->table[hash];
    node** prev = ht->table + hash;
    while(point != NULL && strcmp(point->key, key))
    {
	prev = &((*prev)->next);
	point = point->next;
    }

    if(point == NULL) // insert at the end of the linked list
	*prev = new;
    else // The key is already used
	return 0;
    return 1;
}

/* Delete an element, return 0 if not found */
int hashtbl_remove(hashtbl* ht, const char* key)
{
    /* Find the node */
    int hash = _hash(ht->size, key);
    node* point = ht->table[hash];
    node** prev = ht->table + hash;
    while(point != NULL && strcmp(point->key, key))
    {
	prev = &((*prev)->next);
	point = point->next;
    }

    if(point == NULL) // The element doesn't exist
	return 0;

    *prev = point->next;
    free(point);
    return 1;
}

/* Return the data corresponding to a key, return NULL if not found */
void* hashtbl_find(hashtbl* ht, const char* key)
{
    /* Find the node */
    int hash = _hash(ht->size, key);
    node* point = ht->table[hash];
    while(point != NULL && strcmp(point->key, key))
	point = point->next;

    if(point != NULL)
	return point->data;
    else
	return NULL;
}

/* Print the inside of the hashtable, for debugging purpose */
void hashtbl_print(hashtbl* ht)
{
    /* For each hash (or each collision list) */
    for(int i=0; i < ht->size; i++)
    {
	node* point = ht->table[i];
	if(point != NULL)
	{
	    printf("Hash %i: ", i);
	    while(point != NULL)
	    {
		printf("%s->", point->key);
		point = point->next;
	    }
	    printf("Ø\n");
	}
    }
}
