#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "parser.h"
#include "string.h"

/* Compute the checksum of a line */
int checksum(char* line)
{
    int checksum = 0;
    unsigned int temp = 0;
    for(int i = 0; line[i] != '\0'; i += 2)
    {
	sscanf(line+i, "%02X", &temp);
	checksum += temp;
    }
    return((~checksum) & 0xFF);
}

/* Encode an instruction to S19 and add it to a buffer */
void append_translate(inst* p, char** raw_data, int* buffer_size, int* addr_current)
{
    // First of all, is the raw_data buffer too little ?
    if((addr_current + 8) >= (buffer_size - 1)) // ENLARGE
	*raw_data = realloc(*raw_data, ((*addr_current) + 20) * sizeof(char));
    *buffer_size += 20;

    char trad[8];
    if(p->assembly > 0xFF)
    {
	if(p->mode == INHERANT)
	{
	    *addr_current += 4;
	    sprintf(trad, "%04X", p->assembly);
	}
	else if(p->mode == EXTENDED)
	{
	    *addr_current += 8;
	    sprintf(trad, "%04X%04X", p->assembly, p->data);
	}
	else
	{
	    *addr_current += 6;
	    sprintf(trad, "%04X%02X", p->assembly, p->data);
	}
    }
    else
    {
	if(p->mode == INHERANT)
	{
	    *addr_current += 2;
	    sprintf(trad, "%02X", p->assembly);
	}
	else if(p->mode == EXTENDED)
	{
	    *addr_current += 6;
	    sprintf(trad, "%02X%04X", p->assembly, p->data);
	}
	else
	{
	    *addr_current += 4;
	    sprintf(trad, "%02X%02X", p->assembly, p->data);
	}
    }

    strcat(*raw_data, trad);
}

/* Format the raw data of a program to S19 and write it to a file */
void write_s19(metaprog* meta, char* raw_data, int addr_size, int org)
{
    FILE* file;
    if(!(file= fopen(meta->name, "w+")))
	ERROR("File was not found", 0);

    int pos_end = strlen(raw_data);
    int pos = 0;
    int current_addr = org;
    int line_pos;

    char* line;

    while(pos_end > pos)
    {
	line = calloc(75, sizeof(char));
	line_pos = 2;

	/* Leading S */
	line[0] = 'S';

	/* Byte count */
	if((pos_end - pos) < 63)
	    sprintf(line+2, "%02X", ((pos_end - pos)/2)+2);
	else
	    sprintf(line+2, "%02X", 0x23);

	/* Record type */
	if(addr_size == 16)
	{
	    line[1] = '1';
	    /* Address */
	    sprintf(line + 4, "%04X", current_addr);
	    line_pos += 4;
	}
	else if(addr_size == 24)
	{
	    line[1] = '2';
	    /* Address */
	    sprintf(line + 4, "%08X", current_addr);
	    line_pos += 6;
	}
	else if(addr_size == 32)
	{
	    line[1] = '3';
	    /* Address */
	    sprintf(line + 4, "%010X", current_addr);
	    line_pos += 8;
	}

	line_pos += 2;

	strncpy(line+line_pos, raw_data+pos, 66);
	current_addr += 64;
	pos += 66;

	/* Checksum */
	line_pos = strlen(line);
	sprintf(line+line_pos-2, "%02X", checksum(line+2));

	fprintf(file, "%s\n", line);

	free(line);
    }
    /* End of file */
    fprintf(file, "S9030000FC\n");
}

/* Format a metaprog structure to the S19 format and write it to a file */
int process_all(metaprog* meta)
{
    int buffer_size = 100;
    char* raw_data = calloc(buffer_size, sizeof(char));
    int addr_current = meta->org;
    int n_inst = 0;
    int addr_size;
    if(n_inst < pow(2, 16))
	addr_size = 16;
    else if (n_inst < pow(2, 24))
	addr_size = 24;
    else if (n_inst < pow(2, 32))
	addr_size = 32;
    else
	ERROR("Too many instructions, maximum size is 2^32", meta->lc);

    for(inst* p = meta->start_prog; p != NULL; p = p->next)
    {
	if(p->mnem == LABEL_ONLY)
	    continue;
	append_translate(p, &raw_data, &buffer_size, &addr_current);
    }

    write_s19(meta, raw_data, addr_size, meta->org);

    free(raw_data);
    return 0;
}
