#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "parser.h"
#include "hashtbl.h"
#include "instrutils.h"

/* Give the name (in the form of a string) of the instruction designated by a
 * mnemonic */
char* mnem_to_string(inst_mnem mnem)
{
    switch(mnem)
    {
        case LDAA:return "LDAA"; break;
        case LDAB:return "LDAB"; break;
        case STAA:return "STAA"; break;
        case STAB:return "STAB"; break;
        case ADDA:return "ADDA"; break;
        case ADDB:return "ADDB"; break;
        case ADDD:return "ADDD"; break;
        case SUBA:return "SUBA"; break;
        case SUBB:return "SUBB"; break;
        case SUBD:return "SUBD"; break;
        case BCC: return "BCC"; break;
        case BEQ: return "BEQ"; break;
        case BNE: return "BNE"; break;
        case CMPA:return "CMPA"; break;
        case CMPB:return "CMPB"; break;
        case LSR: return "LSR"; break;
        case LSRA:return "LSRA"; break;
        case LSRB:return "LSRB"; break;
        case LSRD:return "LSRD"; break;
        case LSLA:return "LSLA"; break;
        case LSLB:return "LSLB"; break;
        case LSLD:return "LSLD"; break;
        case ROL: return "ROL"; break;
        case ROLA:return "ROLA"; break;
        case ROLB:return "ROLB"; break;
	case ROR: return "ROR"; break;
        case NAME:return "NAME"; break;
        case ORG: return "ORG"; break;
	case END: return "END"; break;
	case LABEL_ONLY: return "LABEL_ONLY"; break;
	default: return "MNEMONIC NOT FOUND"; break;
    }
}

/* Give the name (a string) of the addressing mode designated by a mnemonic */
char* mode_to_string(addr_mode mode)
{
    switch(mode)
    {
	case IMMEDIATE: return "IMMEDIATE"; break;
	case DIRECT: return "DIRECT"; break;
	case EXTENDED: return "EXTENDED"; break;
	case INDEXED_X: return "INDEXED_X"; break;
	case INDEXED_Y: return "INDEXED_Y"; break;
	case RELATIVE: return "RELATIVE"; break;
	case INHERANT: return "INHERANT"; break;
	default: return "ADDRESSING MODE NOT FOUND"; break;
    }
}

/* Initialize a metaprog structure that will contain informations and code of a
 * program */
metaprog* init_meta()
{
    metaprog* meta = malloc(sizeof(metaprog));

    strcpy(meta->name, "\0");
    meta->org = -1;
    meta->lc = 0;
    meta->labels = hashtbl_create(20);

    return meta;
}

/* Get the next line from a file and put it in the buffer argument.
 * This function takes a pointer to a buffer, a pointer to it's size and an
 * input stream. The buffer must be mallocated 
 * Return 0 if line contains (ends with) EOF */
int my_getline(char** buffer, int* buffer_size, FILE* stream)
{
    int i = 0;
    char c;
    while(((c = fgetc(stream)) != '\n') && (c != '\r') && (c != EOF) && (c != ';'))
    {
	if(c == ';')
	    break;
	if(i >= *buffer_size - 2) // Enlarge your buffer
	{
	    *buffer_size *= 2;
	    *buffer = realloc(*buffer, (*buffer_size) * sizeof(char));
	}
	if(('A' <= c) && (c <= 'Z'))
	    c -= ('A'-'a');
	(*buffer)[i] = c;
	i++;
    }
    if(c == ';')
	for(;((c = fgetc(stream)) != '\n') && (c != '\r') && (c != EOF););
    (*buffer)[i] = '\0';
    return (c != EOF);
}

/* Get a word into buffer like my_getline does, starting at given position.
 * The buffer must be mallocated
 * pos is incremented to next character after the word
 * Return 0 if first character encountered is \0 */
int get_word(char** buffer, int* buffer_size, char* line, int* pos)
{
    if(line[*pos] == '\0')
	return 0;

    // Find \0
    int end_string_pos = 0;
    for(;line[end_string_pos] != '\0'; end_string_pos++);

    int wordsize = 0;
    while(line[*pos] != ' ' && line[*pos] != '\t' && line[*pos] != '\0' && line[*pos] != '\n' && line[*pos] != '\r')
    {
	wordsize++;
	if(wordsize >= *buffer_size - 2) // Enlarge your buffer
	{
	    *buffer_size *= 2;
	    *buffer = realloc(*buffer, (*buffer_size) * sizeof(char));
	}
	(*buffer)[wordsize - 1] = line[*pos];
	(*pos)++;
    }

    // Realloc at exact size
    *buffer = realloc(*buffer, 1 + (wordsize * sizeof(char)));
    *buffer_size = wordsize;
    (*buffer)[wordsize] = '\0';

    return 1;
}

/* Return the position of the next word of a line, starting at given position
 * If there is no next word, position of '\0' is returned */
int skip_blanks(char* line, int start)
{
    int i = start;
    for(; (line[i] == ' ' || line[i] == '\t'); i++);
    return i;
}

/* Add a new instruction in the instruction array of a metaprog structure */
void add_inst(metaprog* meta,
	      char label[MAX_LABEL_SIZE],
	      char name[6],
	      inst_mnem mnem,
	      addr_mode mode,
	      int assembly,
	      int data)
{
    inst* new = malloc(sizeof(inst));
    strcpy(new->label, label);
    strcpy(new->name, name);
    new->mnem = mnem;
    new->mode = mode;
    new->assembly = assembly;
    new->data = data;
    new->next = NULL;

    if(!meta->start_prog)
	meta->start_prog = new;
    else
	meta->end_prog->next = new;

    meta->end_prog = new;
}

/* Get and handle the label of a line (if any)
 * Return the position of first letter of next word
 * (or the position of \0 if the label is alone or the line empty) */
int parse_label(char* line, metaprog* meta)
{
    int i;
    if((i = skip_blanks(line,0)) || (line[i] == '\0')) // no label;
	return(i);

    char* label = calloc(5, sizeof(char));
    int label_size = 5;
    get_word(&label, &label_size, line, &i);

    // Remove colon if any
    if(line[i-1] == ':')
    {
	label_size--;
	label[label_size] = '\0';
    }

    // Check label size
    if(label_size > MAX_LABEL_SIZE)
    {
	free(label);
	ERROR("Label too long", meta->lc);
	return label_size;
    }

    add_inst(meta, label, "", LABEL_ONLY, 0, 0x00, 0x00);
    free(label);

    if(line[i-1] == ':')
	return label_size + 1;
    return label_size;
}

/* Get the S19 opcode of an instruction for a certain addressing mode */
int get_assembly(opcode* op, addr_mode mode)
{
    switch(mode)
    {
	case IMMEDIATE : return op->imm; break;
	case DIRECT    : return op->dir; break;
	case EXTENDED  : return op->ext; break;
	case INDEXED_X : return op->indx; break;
	case INDEXED_Y : return op->indy; break;
	case RELATIVE  : return op->rel; break;
	case INHERANT  : return op->inh; break;
	default: return -1; break;
    }
}

/* Understand (mostly get addressing mode) and properly add an instruction to
 * the metaprog structure */
void handle_inst(opcode* op, char* arg, metaprog* meta)
{
    int data_start = 0;
    addr_mode mode = DIRECT;

    if(arg[0] == '#')
    {
	mode = IMMEDIATE;
	data_start++;
    }

    int data;
    char* end_data;
    char c = arg[data_start];
    switch(c)
    {
	case '$': data = strtol(arg + (++data_start), &end_data, 16); break;
	case '@': data = strtol(arg + (++data_start), &end_data, 8); break;
	case '%': data = strtol(arg + (++data_start), &end_data, 2); break;
	default: data = strtol(arg + data_start, &end_data, 10); break;
    }

    //relative
    if((op->mnem == BCC) || (op->mnem == BEQ) || (op->mnem == BNE))
    {
	if(mode == IMMEDIATE)
	    ERROR("Can't use immediate mode with branching instructions", meta->lc);
	mode = RELATIVE;
    }
    else if(*end_data == ',') // Indexed
    {
	if(mode == IMMEDIATE)
	    ERROR("Can't use immediate mode with indexed mode", meta->lc);

	if(*(end_data+1) == 'x')
	    mode = INDEXED_X;
	else if(*(end_data+1) == 'y')
	    mode = INDEXED_Y;
	else
	    ERROR("Unauthorized register for indexed mode", meta->lc);
    }

    if(mode == DIRECT && data > 0xFF)
	mode = EXTENDED;

    add_inst(meta, "", op->name, op->mnem, mode, get_assembly(op, mode), data);
}

/* Understand a line, return 0 when assembly directive 'end' is reached */
int parse_line(char* line, metaprog* meta)
{
    if(line[0] == '*') // This line is a comment, ignore
	return 1;

    /* Get and handle the label (if any) */
    int i = parse_label(line, meta);
    i = skip_blanks(line, i);

    int w_size = 5;
    char* w = calloc(w_size, sizeof(char));

    // Opcode
    if(!get_word(&w, &w_size, line, &i)) // No instruction
    {
	free(w);
	return 1;
    }

    opcode* op;
    if(!(op = hashtbl_find(OPCODES, w)))
    {
       ERROR("unknown opcode", meta->lc);
       exit(0);
    }

    i = skip_blanks(line, i);

    // Assembly directives
    if(op->mnem == NAME)
    {
	char* temp = calloc(10, sizeof(char));
	int name_size = 10;
	if(!get_word(&(temp), &name_size, line, &i))
	    ERROR("No name was given with name directive", meta->lc);
        else if(name_size >= 256)
            ERROR("The name given with name directive was too long (max is 255 characters)", meta->lc);
        else
            strcpy(meta->name, temp);
    }
    else if(op->mnem == ORG)
    {
	switch(line[i])
	{
	    case '$': meta->org = strtol(line + i + 1, NULL, 16); break;
	    case '@': meta->org = strtol(line + i + 1, NULL, 8); break;
	    case '%': meta->org = strtol(line + i + 1, NULL, 2); break;
	    default: meta->org = strtol(line + i, NULL, 10); break;
	}
    }
    else if(op->mnem == END)
    {
	free(w);
	return 0;
    }
    else
    {
	// Argument
	if(op->narg == 0)
	{
	    if(line[i] != '\0')
		ERROR("Operand given for an Inherent instruction", meta->lc);
	    else // Inherant
		add_inst(meta, "", op->name, op->mnem, INHERANT, op->inh, 0);
	}
	else
	{
	    if(!get_word(&w, &w_size, line, &i))
		ERROR("An argument was expected", meta->lc);

	    handle_inst(op, w, meta);
	}
    }

    free(w);
    return 1;
}

/* Parse a file, assemble the instructions, create and correctly fill a metaprog
 * structure describing the whole assembled program */
metaprog* parse(FILE* file)
{
    metaprog* meta = init_meta();

    int line_size = 20;
    char* line = malloc(line_size);
    while(my_getline(&line, &line_size, file))
    {
	meta->lc++;
	if(!parse_line(line, meta))
	    break;
    }

    free(line);
    return(meta);
}

/* Print the content of a metaprog structure, for debugging purpose */
void meta_print(metaprog* meta)
{
    printf("Last line number: %i\n", meta->lc);

    printf("Labels: \n###\n");
    hashtbl_print(meta->labels);
    printf("###\n");

    printf("Origin: %i\n", meta->org);
    printf("Output name: %s\n", meta->name);

    printf("Instruction list:\n");
    printf("-\n");
    inst* p = meta->start_prog;
    while(p != NULL)
    {
	printf("Label: %s\n", p->label);
	printf("Name: %s\n", p->name);
	printf("Mnemonic: %s\n", mnem_to_string(p->mnem));
	printf("Addressing mode: %s\n", mode_to_string(p->mode));
	printf("Assembled version: %X\n", p->assembly);
	printf("Data: %X\n", p->data);
	printf("-\n");

	p = p->next;
    }
}

/* Properly free ressources used by a metaprog structure */
void metaprog_destroy(metaprog* meta)
{
    hashtbl_destroy(meta->labels);
    free(meta->start_prog);
    free(meta);
}
