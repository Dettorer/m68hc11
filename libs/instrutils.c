#include <stdlib.h>
#include "hashtbl.h"
#include "instrutils.h"
#include "parser.h"

/* Initialise the global hashtable that contains instructions informations */
void make_inst()
{
    /* Let's initialize an hashtable with the exact number of instructions, to
     * do so we simply count the number of appearance of the macro in instlist.h
     */
    #define INST(...) +1
    OPCODES = hashtbl_create(0
	#include "instlist.h"
	);
    #undef INST

    /* Then we actually use the informations in instlist.h to add the
     * instructions in the hashtable */
    #define INST(name, mnem, narg, imm, dir, ext, indx, indy, rel, inh) \
	hashtbl_add(OPCODES, \
	  name, \
	  &((opcode) {name, mnem, narg, imm, dir, ext, indx, indy, rel, inh}), \
	  sizeof(opcode));
    #include "instlist.h"
    #undef INST
}
