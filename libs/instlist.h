/* This file contains the definition of each supported instraction : it's name
 * (a string), it's mnemonic (an enumeration value), it's number of expected
 * arguments and it's S19 opcode for every addressing mode. A -1 indicates that
 * the corresponding addressing mode cannot be used with this instruction.
 *
 * The order of the arguments of that macro is the following :
 * name, mnemonic, nargs, imm,  dir,  ext,  indx, indy,    rel, inh
 */

/* Accumulators */
INST("ldaa", LDAA,    1,     0x86, 0x96, 0xB6, 0xA6, 0x18A6,   -1, -1)
INST("ldab", LDAB,    1,     0xC6, 0xD6, 0xF6, 0xE6, 0x18E6,   -1, -1)

INST("staa", STAA,    1,     -1,   0x97, 0xB7, 0xA7, 0x18A7,   -1, -1)
INST("stab", STAB,    1,     -1,   0xD7, 0xF7, 0xE7, 0x18E7,   -1, -1)

/* Add and Sub */
INST("adda", ADDA,    1,     0x8B, 0x9B, 0xBB, 0xAB, 0x18AB,   -1, -1)
INST("addb", ADDB,    1,     0xCB, 0xDB, 0xFB, 0xEB, 0x18EB,   -1, -1)
INST("addd", ADDD,    1,     0xC3, 0xD3, 0xF3, 0xE3, 0x18E3,   -1, -1)

INST("suba", SUBA,    1,     0x80, 0x90, 0xB0, 0xA0, 0x18A0,   -1, -1)
INST("subb", SUBB,    1,     0xC0, 0xD0, 0xF0, 0xE0, 0x18E0,   -1, -1)
INST("subd", SUBD,    1,     0x83, 0x93, 0xB3, 0xA3, 0x18A3,   -1, -1)

// Branching
INST("bcc",  BCC,     1,       -1,   -1,   -1,   -1,     -1, 0x24, -1)
INST("beq",  BEQ,     1,       -1,   -1,   -1,   -1,     -1, 0x27, -1)
INST("bne",  BNE,     1,       -1,   -1,   -1,   -1,     -1, 0x2C, -1)

// CMP
INST("cmpa", CMPA,    1,     0x81, 0x91, 0xB1, 0xA1, 0x18A1,   -1, -1)
INST("cmpb", CMPB,    1,     0xC1, 0xD1, 0xF1, 0xE1, 0x18E1,   -1, -1)

// Logical shifts
INST("lsr",  LSR,     1,       -1,   -1, 0x74, 0x64, 0x1874,   -1, -1)
INST("lsra", LSRA,    0,       -1,   -1,   -1,   -1,   -1,     -1, 0x44)
INST("lsrb", LSRB,    0,       -1,   -1,   -1,   -1,   -1,     -1, 0x54)
INST("lsrd", LSRD,    0,       -1,   -1,   -1,   -1,   -1,     -1, 0x04)

INST("lsla", LSLA,    0,       -1,   -1,   -1,   -1,   -1,     -1, 0x48)
INST("lslb", LSLB,    0,       -1,   -1,   -1,   -1,   -1,     -1, 0x58)
INST("lsld", LSLD,    0,       -1,   -1,   -1,   -1,   -1,     -1, 0x05)

// Rotations
INST("rol",  ROL,     1,       -1,   -1, 0x79, 0x69, 0x1869,   -1, -1)
INST("rola", ROLA,    0,       -1,   -1,   -1,   -1,     -1,   -1, 0x49)
INST("rolb", ROLB,    0,       -1,   -1,   -1,   -1,     -1,   -1, 0x59)

// Others
INST("ror",  ROR,     1,       -1,   -1, 0x76, 0x66, 0x1866,   -1, -1)

// Assembler directives
INST("name", NAME,    1,       -1,   -1,   -1,   -1,     -1,   -1, -1)
INST("org",  ORG,     1,       -1,   -1,   -1,   -1,     -1,   -1, -1)
INST("end",  END,     0,       -1,   -1,   -1,   -1,     -1,   -1, -1)
