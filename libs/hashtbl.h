#ifndef _HASHBTL_H_
# define _HASHBTL_H_

#include "../constants.h"

int hash(int size, char* str);

/* Collision list */
typedef struct node
{
    char key[MAX_LABEL_SIZE];
    void* data;
    struct node* next;
} node;

/* Hash table */
typedef struct
{
    int size;
    node** table;
} hashtbl;

hashtbl* hashtbl_create(int size);
void hashtbl_destroy(hashtbl* ht);
int hashtbl_add(hashtbl* ht, const char* key, void* data, size_t data_size);
int hashtbl_remove(hashtbl* ht, const char* key);
void* hashtbl_find(hashtbl* ht, const char* key);
void hashtbl_print(hashtbl* ht);

#endif
