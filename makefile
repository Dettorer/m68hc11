NAME := m68hc11

DEPS_DIR := libs
_DEPS := hashtbl.c parser.c instrutils.c s19.c
DEPS := $(foreach dep,$(_DEPS),$(DEPS_DIR)/$(dep))
DEPS_HEADER := $(subst .c,.h,$(DEPS))

CC := gcc
CFLAGS := -Wall -Wextra -pedantic -ansi --std=c99 -g -I$(DEPS_DIR)
COMPILE := $(CC) $(CFLAGS)

TEST_SOURCE := test.c
TEST := test.out

all: $(DEPS) $(DEPS_HEADER)
	@echo Compiling...
	@$(COMPILE) -o $(NAME) $(NAME).c $(DEPS)
	@echo Done.

test: $(TEST_SOURCE) $(DEPS) $(DEPS_HEADER)
	@$(COMPILE) -o $(TEST) $(TEST_SOURCE) $(DEPS)
	@./$(TEST)

clean:
	@echo Cleaning...
	@rm -f $(TEST)
	@echo Done.
