#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libs/parser.h"
#include "libs/instrutils.h"
#include "libs/s19.h"

int main()
{
    /* Menu */
    printf("Please enter the input file's name\n>");

    size_t buffer_size = 10;
    char* filename = calloc(buffer_size, sizeof(char));
    char c;
    unsigned int i = 0;
    while((c = getchar()) != '\n')
    {
	if(i >= buffer_size)
	    filename = realloc(filename, (buffer_size *= 2) * sizeof(char));
	filename[i++] = c;
    }
    putchar('\n');
    filename = realloc(filename, (i+2) * sizeof(char));
    filename[i+1] = '\0';

    /* Initialise environment */
    make_inst();
    FILE* input_file = fopen(filename, "r");

    /* Assemble data file */
    metaprog* meta = parse(input_file);

    printf("Output file is %s\n", meta->name);
    if(!strcmp(meta->name, "\0"))
    {
        ERROR("No name directive was found", 0);
        exit(0);
    }
    if(meta->org == -1)
    {
        ERROR("No org directive was found", 0);
        exit(0);
    }

    /* Encode data to S19 format */
    process_all(meta);

    return 0;
}
